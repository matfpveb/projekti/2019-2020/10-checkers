import {
  Component,
  OnInit,
} from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { CheckersService } from '../services/checkers.service';
import { Subscription } from 'rxjs';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css'],
})
export class PlayerListComponent implements OnInit {
  playerList$: Subscription;
  playerList = [];

  sortedData = [];
  searchText = '';

  constructor(pipe: DecimalPipe, private checkersService: CheckersService) {
    this.sortedData = this.playerList.slice();
  }

  ngOnInit(): void {
    this.playerList = this.checkersService.getPlayerList();
    this.sortedData = this.playerList.sort((a,b) => b.elo - a.elo);
}

showSorted(num: number) {
  switch (num) {
    case 5:
        this.sortedData = this.playerList.sort((a,b) => b.elo - a.elo).slice(0, 5);
      break;
    case 10:
        this.sortedData = this.playerList.sort((a,b) => b.elo - a.elo).slice(0, 10);
      break;
    case -5:
        this.sortedData = this.playerList.sort((a,b) => b.elo - a.elo).slice(this.playerList.length - 5, this.playerList.length);
      break;
    case -10:
        this.sortedData = this.playerList.sort((a,b) => b.elo - a.elo).slice(this.playerList.length - 10, this.playerList.length);
      break;
    case 0:
        this.sortedData = this.playerList.sort((a,b) => b.elo - a.elo);

  }
}
  
  sortData(sort: Sort) {
    const data = this.playerList;
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'index':
          return this.compare(a.index, b.index, isAsc);
        case 'name':
          return this.compare(a.name, b.name, isAsc);
        case 'elo':
        case 'won':
          return this.compare(a.won, b.won, isAsc);
        case 'lost':
          return this.compare(a.lost, b.lost, isAsc);
        case 'score':
          return this.compare(a.won - a.lost, b.won - b.lost, isAsc);
        default:
          return 0;
      }
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
