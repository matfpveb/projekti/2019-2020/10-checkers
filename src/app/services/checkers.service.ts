import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler } from '@angular/common/http';
import mockdata from "../mock-database.json"
@Injectable({
  providedIn: 'root',
})
export class CheckersService {
  constructor(
    private httpclient: HttpClient,
    next: HttpHandler
  ) {}

  getPlayerList():any {
    return mockdata;
  }

}
