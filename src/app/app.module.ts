import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CheckersService } from './services/checkers.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PlayerListComponent } from './player-list/player-list.component';
import { DecimalPipe } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSortModule } from '@angular/material/sort';
import { FilterPipe } from './filter.pipe';
import { HomeComponent } from './home/home.component';
import { BoardComponent } from './board/board.component';
import { LoaderComponent } from './loader/loader.component';
import { RegisterComponent } from './register/register.component';
import { SignComponent } from './sign/sign.component';

@NgModule({
  declarations: [
    AppComponent,
    PlayerListComponent,
    FilterPipe,
    HomeComponent,
    BoardComponent,
    LoaderComponent,
    RegisterComponent,
    SignComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSortModule
  ],
  providers: [
    CheckersService,
    PlayerListComponent,
    DecimalPipe
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
