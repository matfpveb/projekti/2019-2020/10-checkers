# Project 10-Checkers

"Dame" browser board game u stilu lichess.org sajta

Pokretanje angular komponente nakon kloniranja projekta i pozicioniranja u njegovom direktorijumu:
- npm install
- ng serve
- Pokrenuti aplikaciju na http://localhost:4200
## Developers

- [Strahinja Ivanovic, 149/2015](https://gitlab.com/Aleksej10)
- [Stefan Stefanovic, 26/2016](https://gitlab.com/StefaniusSuperbus)
- [Filip Kristic, 335/2015](https://gitlab.com/Vulkin996)
- [Stefan Jacovic, 503/2017](https://gitlab.com/StefanJ996)
